<?php
/**
 * @file
 * debut_enhanced_security.default_password_policy.inc
 */

/**
 * Implements hook_default_password_policy().
 */
function debut_enhanced_security_default_password_policy() {
  $export = array();

  $password_policy = new stdClass();
  $password_policy->disabled = FALSE; /* Edit this to true to make a default password_policy disabled initially */
  $password_policy->api_version = 1;
  $password_policy->name = 'Default Password Policy';
  $password_policy->description = 'Password policy for all roles';
  $password_policy->enabled = TRUE;
  $password_policy->policy = array(
    'uppercase' => '1',
    'alphanumeric' => '1',
    'complexity' => '2',
    'lowercase' => '1',
    'length' => '8',
    'history' => '2',
    'punctuation' => '0',
    'letter' => '1',
    'delay' => '0',
    'digit_placement' => '0',
    'username' => '1',
    'digit' => '1',
  );
  $password_policy->created = 1335291730;
  $password_policy->expiration = 0;
  $password_policy->warning = '';
  $password_policy->weight = FALSE;
  $export['Default Password Policy'] = $password_policy;

  return $export;
}
