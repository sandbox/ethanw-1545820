<?php
/**
 * @file
 * debut_enhanced_security.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function debut_enhanced_security_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "password_policy" && $api == "default_password_policy") {
    return array("version" => "1");
  }
}
